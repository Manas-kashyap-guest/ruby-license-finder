require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = FileList['./spec/**/*_spec.rb'] - FileList['./spec/lib/license_finder/package_managers/*bower*_spec.rb'] - FileList['./spec/lib/license_finder/package_managers/*go_dep*_spec.rb']
end
